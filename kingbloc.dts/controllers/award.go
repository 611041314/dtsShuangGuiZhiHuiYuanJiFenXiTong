package controller

import (
	"kingbloc.dts/services"
	"net/http"
)

func CalcAward(rep http.ResponseWriter, req *http.Request) {

	userA := validateUserInfo(req)

	if userA == nil {
		rep.WriteHeader(403)
		return
	}

	myAccount := service.FindAccountBySSOId(userA.Id)

	if myAccount == nil || myAccount.Id == 0 {
		rep.WriteHeader(403)
		return
	}

	//new(service.Award).CalcAwardLayer() // 报单时立即计算
	new(service.Award).CalcAwardAmount()
	new(service.Award).CalcAwardLucky()
	new(service.Award).CalcAwardManagement()
	new(service.Award).CalcAwardCharm(myAccount) // 单独计算

	msg := &service.Msg{Topic: "test", Method: "CalcAwarIsOk", Data: "量碰，幸运，管理奖计算完毕。。"}
	service.Publish(msg)

	rep.WriteHeader(200)

}
