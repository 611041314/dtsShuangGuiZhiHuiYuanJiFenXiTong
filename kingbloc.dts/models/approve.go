package model

import (
	"time"
)

type Approve struct {
	Id                 int64     `json:"id"`
	ProposerAccountId  int64     `json:"proposerAccountId"`  // 申请人 id
	ProposerAccountMob string    `json:"proposerAccountMob"` // 申请人手机
	ExaminerAccountId  int64     `json:"examinerAccountId"`  // 审批者 id
	ExaminerAccountMob string    `json:"examinerAccountMob"` // 审批者手机
	Types              int8      `json:"types"`              // 账户类型: 0 密码修改
	Status             int8      `json:"status"`             // 0 开始，1结束
	DataContent        string    `json:"dataContent"`
	CreateTime         time.Time `json:"createTime"` // 创建时间日志
}

func NewApprove() *Approve {

	ap := new(Approve)
	ap.CreateTime = time.Now()
	return ap

}

func (ac *Approve) ByExaminerAccountIdId(id int64) []*Approve {
	acs := make([]*Approve, 0)
	MysqlEngine.Where("examiner_account_id=? and status = 0", id).Find(&acs)
	return acs
}

func (ac *Approve) ByProposerAccountIdId(id int64) []*Approve {
	acs := make([]*Approve, 0)
	MysqlEngine.Where("proposer_account_id=? and status = 0", id).Find(&acs)
	return acs
}

func (ac *Approve) ById(id int64) *Approve {
	MysqlEngine.Id(id).Get(ac)
	return ac
}

func (ap *Approve) ByIdAndAid(apid, aid int64) *Approve {

	MysqlEngine.Id(apid).Where("examiner_account_id = ?", aid).Get(ap)
	if ap.Id == 0 {
		return nil
	}
	return ap
}

func (ap *Approve) Add() {

	MysqlEngine.Where("proposer_account_id = ? and types = 0 and status = 0", ap.ProposerAccountId).Get(ap)
	if ap.Id > 0 {
		return
	}
	//fmt.Printf("%+v\n", ap)
	MysqlEngine.InsertOne(ap)

}

func (ap *Approve) Update() {
	MysqlEngine.Id(ap.Id).Update(ap)
}

func (ap *Approve) UpdateCols(column ...string) {
	MysqlEngine.Id(ap.Id).Cols(column...).Update(ap)
}
