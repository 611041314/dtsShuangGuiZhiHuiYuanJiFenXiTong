package model

import (
	"time"
)

// 订单
type Orderr struct {
	Id              int64     // 订单 id
	ParentId        int64     // 上级 id
	IntroducerId    int64     // 推荐人 account id
	AccountId       int64     // 定单主人 id
	Layer           int       // 全局的层数
	Price           float64   // 报单金额
	AwardCount      float64   // 总奖金
	AwardLayer      float64   // 层奖
	AwardAmount     float64   // 量奖
	AwardLucky      float64   // 幸运奖
	AwardManagement float64   // 管理奖
	AwardCharm      float64   // 魅力奖
	CreateAt        time.Time // 创建时间
	Status          int8      // 状态
	Sort            int64     // 当前层的排序 sort by row count
}

func NewOrder() *Orderr {

	order := new(Orderr)
	order.CreateAt = time.Now()
	order.Status = 0
	return order

}

func (o *Orderr) ByAccountId(accountId int64) []*Orderr {
	orders := make([]*Orderr, 0)
	MysqlEngine.Where("account_id = ?", accountId).OrderBy("create_at").Find(&orders)
	return orders
}

// 根据推荐人 account id 找 orders
func (o *Orderr) ByIntroducerId(accountId int64) []*Orderr {
	orders := make([]*Orderr, 0)
	MysqlEngine.Where("introducer_id = ?", accountId).OrderBy("create_at").Find(&orders)
	return orders
}

func (o *Orderr) NextFloorOrdersByOrderId(orderId int64) []*Orderr {
	orders := make([]*Orderr, 0)
	MysqlEngine.Where("parent_id = ?", orderId).OrderBy("create_at").Find(&orders)
	return orders
}

// 前面 orders
func (o *Orderr) OrdersByBeforeTime(time time.Time, count int) (orders []*Orderr) {
	orders = make([]*Orderr, 0)
	MysqlEngine.Where("create_at < ?", time).Limit(count).Find(&orders)
	return orders
}

// 之后的 orders
func (o *Orderr) OrdersByAfterTime(time time.Time, count int) (orders []*Orderr) {
	orders = make([]*Orderr, 0)
	MysqlEngine.Where("create_at > ?", time).Limit(count).Find(&orders)
	return orders
}

// 逐层总数
func (order *Orderr) LayerByRowCount(layer int) int64 {
	or := new(Orderr)
	sum, _ := MysqlEngine.Where("layer=?", layer).Count(or)
	or = nil
	return sum
}

func (order *Orderr) ById(id int64) *Orderr {
	MysqlEngine.Id(id).Get(order)
	return order
}

// 下层的第一个Order
func (order *Orderr) NextFloorFirstOrderById(orderId int64) {
	MysqlEngine.Where("parent_id = ?", orderId).OrderBy("create_at").Limit(1).Get(order)
}

func (o *Orderr) Count() int64 {
	count, _ := MysqlEngine.Count(new(Orderr))
	return count
}

func (o *Orderr) Add() {

	MysqlEngine.InsertOne(o)

}

func (o *Orderr) UpdateAwardLayer() {
	o.AwardCount += o.AwardLayer
	MysqlEngine.Id(o.Id).Cols("award_layer", "award_count").Update(o)

}

func (o *Orderr) UpdateAmountAward() {
	o.AwardCount += o.AwardAmount
	MysqlEngine.Id(o.Id).Cols("award_amount", "award_count").Update(o)
}

func (o *Orderr) UpdateLuckyAward() {
	o.AwardCount += o.AwardLucky
	MysqlEngine.Id(o.Id).Cols("award_lucky", "award_count").Update(o)
}

func (o *Orderr) UpdateManagementAward() {
	o.AwardCount += o.AwardManagement
	MysqlEngine.Id(o.Id).Cols("award_management", "award_count").Update(o)
}
func (o *Orderr) UpdateCharmAward() {
	o.AwardCount += o.AwardCharm
	MysqlEngine.Id(o.Id).Cols("award_charm", "award_count").Update(o)
}
